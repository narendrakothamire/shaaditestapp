package com.techguys.shaadiapptest

import android.app.Application
import com.techguys.shaadiapptest.di.components.AppComponent
import com.techguys.shaadiapptest.di.components.DaggerAppComponent
import com.techguys.shaadiapptest.di.modules.AppModule
import com.techguys.shaadiapptest.di.modules.NetModule
import com.techguys.shaadiapptest.data.remote.BASE_URL

class ShaadiTestApplication: Application(){

    private lateinit var appComp: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComp = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule(BASE_URL))
            .build()
        appComp.inject(this)
    }

    fun getAppComp(): AppComponent = appComp


}