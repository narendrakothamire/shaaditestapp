package com.techguys.shaadiapptest.di.components

import com.techguys.shaadiapptest.ShaadiTestApplication
import com.techguys.shaadiapptest.di.modules.AppModule
import com.techguys.shaadiapptest.di.modules.NetModule
import com.techguys.shaadiapptest.di.scopes.AppScope
import dagger.Component
import javax.inject.Singleton

@AppScope
@Singleton
@Component(modules = [AppModule::class, NetModule::class])
interface AppComponent {

    fun plusViewComponent(): ViewComponent

    fun inject(app: ShaadiTestApplication)
}