package com.techguys.shaadiapptest.di.modules

import com.techguys.shaadiapptest.data.IMatchesRepository
import com.techguys.shaadiapptest.data.MatchesRepository
import com.techguys.shaadiapptest.di.scopes.ViewScope
import com.techguys.shaadiapptest.data.remote.WebService
import dagger.Module
import dagger.Provides


@Module
class RepositoryModule {

    @Provides
    @ViewScope
    internal fun provideMatchesRepository(webService: WebService): IMatchesRepository {
        return MatchesRepository(webService)
    }

}