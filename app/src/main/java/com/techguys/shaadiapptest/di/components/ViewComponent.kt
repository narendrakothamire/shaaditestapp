package com.techguys.shaadiapptest.di.components

import com.techguys.shaadiapptest.di.modules.RepositoryModule
import com.techguys.shaadiapptest.di.scopes.ViewScope
import com.techguys.shaadiapptest.ui.MainActivity
import dagger.Subcomponent

@ViewScope
@Subcomponent(modules = [RepositoryModule::class])
interface ViewComponent {

    fun inject(mainActivity: MainActivity)
}
