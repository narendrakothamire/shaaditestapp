package com.techguys.shaadiapptest.data.remote

import com.techguys.shaadiapptest.models.Response
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface WebService{


    @GET("api/")
    fun listMatches(@Query("results") result: Int): Call<Response>

}