package com.techguys.shaadiapptest.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.techguys.shaadiapptest.models.Response
import com.techguys.shaadiapptest.data.remote.WebService
import retrofit2.Call
import retrofit2.Callback
import javax.inject.Inject

class MatchesRepository @Inject constructor(private val webService: WebService): IMatchesRepository {

    override fun getMatches(): LiveData<IMatchesRepository.MatchesResponse> {
        val matchesLiveData = MutableLiveData<IMatchesRepository.MatchesResponse>()
        webService.listMatches(10).enqueue(object : Callback<Response> {
            override fun onFailure(call: Call<Response>, t: Throwable) {
                matchesLiveData.value = IMatchesRepository.MatchesResponse.Error(t)
            }

            override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                if (response.isSuccessful) {
                    val body = response.body()
                    body?.let {
                        matchesLiveData.value = IMatchesRepository.MatchesResponse.Success(body.results!!)
                    }
                }
            }

        })
        return matchesLiveData
    }


}