package com.techguys.shaadiapptest.data

import androidx.lifecycle.LiveData
import com.techguys.shaadiapptest.models.ResultsItem

interface IMatchesRepository {
    fun getMatches(): LiveData<IMatchesRepository.MatchesResponse>

    sealed class MatchesResponse {
        data class Success(val matches: List<ResultsItem>) : IMatchesRepository.MatchesResponse()
        data class Error(val error: Throwable) : IMatchesRepository.MatchesResponse()
    }
}