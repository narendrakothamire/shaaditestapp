package com.techguys.shaadiapptest.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.techguys.shaadiapptest.data.IMatchesRepository
import com.techguys.shaadiapptest.data.MatchesRepository
import com.techguys.shaadiapptest.ui.base.BaseViewModel
import javax.inject.Inject

class MainViewModel(private val matchesRepository: MatchesRepository) : BaseViewModel(){

    private lateinit var response: LiveData<IMatchesRepository.MatchesResponse>

    fun refreshMatches(){
        response = matchesRepository.getMatches()
    }

    fun getMatches(): LiveData<IMatchesRepository.MatchesResponse>{
       return response
    }


    class MainViewModelFactory @Inject constructor(private val matchesRepository: MatchesRepository) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MainViewModel(matchesRepository) as T
        }

    }
}