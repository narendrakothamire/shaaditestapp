package com.techguys.shaadiapptest.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.techguys.shaadiapptest.R
import com.techguys.shaadiapptest.data.IMatchesRepository
import com.techguys.shaadiapptest.di.components.AppComponent
import com.techguys.shaadiapptest.ui.base.BaseActivity
import com.techguys.shaadiapptest.ui.common.RecyclerViewAnimator
import com.techguys.shaadiapptest.ui.common.RecyclerViewOnItemClickListener
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), RecyclerViewOnItemClickListener {

    @Inject
    lateinit var mainViewModelFactory: MainViewModel.MainViewModelFactory

    private lateinit var viewAdapter: MatchesRecyclerViewAdapter
    private lateinit var mainViewModel: MainViewModel

    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.plusViewComponent().inject(this)
    }

    override fun getActivityLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        setupRecyclerView()
        setupViewModel(savedInstanceState)

    }

    private fun setupViewModel(savedInstanceState: Bundle?) {

        mainViewModel = ViewModelProviders.of(this, mainViewModelFactory).get(MainViewModel::class.java)

        if (savedInstanceState == null) mainViewModel.refreshMatches()

        mainViewModel.getMatches().observe(this,
            Observer<IMatchesRepository.MatchesResponse> { response ->
                when (response) {
                    is IMatchesRepository.MatchesResponse.Success -> {
                        progressBar.visibility = View.GONE
                        viewAdapter.addItems(response.matches)
                        viewAdapter.notifyDataSetChanged()
                    }
                    is IMatchesRepository.MatchesResponse.Error -> {
                        progressBar.visibility = View.GONE
                        //TODO Handle error
                    }
                }

            })
    }

    private fun setupRecyclerView() {
        viewAdapter = MatchesRecyclerViewAdapter()
        viewAdapter.recyclerViewOnItemClickListener = this

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            itemAnimator = RecyclerViewAnimator()
            adapter = viewAdapter
        }
    }

    override fun onItemClick(position: Int) {
        viewAdapter.removeItem(position)
        viewAdapter.notifyItemRemoved(position)
        //TODO code to remove data from server
    }

}
