package com.techguys.shaadiapptest.ui.common

interface RecyclerViewOnItemClickListener {
    fun onItemClick(position: Int)
}