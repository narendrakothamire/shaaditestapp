package com.techguys.shaadiapptest.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()