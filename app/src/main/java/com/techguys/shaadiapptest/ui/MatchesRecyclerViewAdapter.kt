package com.techguys.shaadiapptest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.techguys.shaadiapptest.R
import com.techguys.shaadiapptest.ui.common.RecyclerViewOnItemClickListener
import com.techguys.shaadiapptest.databinding.ItemMatchesListBinding
import com.techguys.shaadiapptest.models.ResultsItem

class MatchesRecyclerViewAdapter :
    RecyclerView.Adapter<MatchesRecyclerViewAdapter.MatchesRVHolder>() {

    private val matchesList: MutableList<ResultsItem> = mutableListOf()
    private var inflater: LayoutInflater? = null
    var recyclerViewOnItemClickListener: RecyclerViewOnItemClickListener? = null

    fun addItems(matches: List<ResultsItem>) {
        matchesList.clear()
        matchesList.addAll(matches)
    }

    fun removeItem(position: Int) {
        matchesList.removeAt(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchesRVHolder {
        if (inflater == null) inflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ItemMatchesListBinding>(
                inflater!!,
                R.layout.item_matches_list, parent, false
            )

        return MatchesRVHolder(
            binding,
            recyclerViewOnItemClickListener
        )
    }

    override fun getItemCount(): Int = matchesList.size

    override fun onBindViewHolder(holder: MatchesRVHolder, position: Int) {
        holder.binding.viewModel = matchesList[position]
        holder.binding.executePendingBindings()
    }

    override fun onViewRecycled(holder: MatchesRVHolder) {
        super.onViewRecycled(holder)
        holder.binding.profilePicImageView.setImageResource(0)
    }


    class MatchesRVHolder(
        val binding: ItemMatchesListBinding,
        private val recyclerViewOnItemClickListener: RecyclerViewOnItemClickListener?
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            val clickListener: (View) -> Unit = {
                recyclerViewOnItemClickListener?.onItemClick(adapterPosition)
            }
            binding.declineImageView.setOnClickListener(clickListener)
            binding.connectImageView.setOnClickListener(clickListener)
        }
    }
}