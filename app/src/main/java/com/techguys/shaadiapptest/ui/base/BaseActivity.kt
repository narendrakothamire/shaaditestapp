package com.techguys.shaadiapptest.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.techguys.shaadiapptest.ShaadiTestApplication
import com.techguys.shaadiapptest.di.components.AppComponent

abstract class BaseActivity: AppCompatActivity(){

    protected abstract fun injectDependencies(appComponent: AppComponent)

    protected abstract fun getActivityLayout():Int

    protected abstract fun init(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies((application as ShaadiTestApplication).getAppComp())
        setContentView(getActivityLayout())
        init(savedInstanceState)
    }

}