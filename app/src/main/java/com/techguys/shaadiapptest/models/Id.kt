package com.techguys.shaadiapptest.models


import com.google.gson.annotations.SerializedName


data class Id(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("value")
	val value: Any? = null
)