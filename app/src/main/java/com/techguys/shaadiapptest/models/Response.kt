package com.techguys.shaadiapptest.models


import com.google.gson.annotations.SerializedName


data class Response(

	@field:SerializedName("results")
	val results: List<ResultsItem>? = null,

	@field:SerializedName("info")
	val info: Info? = null
)